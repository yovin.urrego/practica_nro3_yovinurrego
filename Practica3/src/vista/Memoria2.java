/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista;

import java.util.Scanner;

/**
 *
 * @author darkangel
 */
public class Memoria2 {

    //public final static int OVERFLOW = 200000000;
    private String salida = "";
    private int tamanio;
    String tempOOM = "";

    public Memoria2(int leng) {
        this.tamanio = leng;
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i < (leng)) {
            i++;
            try {
                sb.append("a");

            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                break;
            }
        }
        this.salida = String.valueOf(sb);
        this.salida = tempOOM;
    }

    public int getLength() {
        return tamanio;
    }

    public String getOom() {
        return salida;
    }

    public static void main(String[] args) {
        Memoria2 javaHeapTest = new Memoria2(200000000);
        System.out.println(javaHeapTest.getOom().length());
        Scanner sc = new Scanner(System.in);
        System.out.println("precione cualquier numero");
        sc.nextInt();
    }
}
